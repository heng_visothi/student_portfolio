// auth.service.ts
import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';  // Update import statement
import { firstValueFrom } from 'rxjs';
import { URLSearchParams } from 'url';
import * as jwt from 'jsonwebtoken';
import { JwtPayload } from 'jsonwebtoken';// Import JwtPayload type
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(private jwtService: JwtService, private http: HttpService) {}

  async login(username: string, password: string) {
    const { data } = await firstValueFrom(
      this.http.post(
        'https://signin.sala.tech/realms/sandbox/protocol/openid-connect/token',
        new URLSearchParams({
          client_id: 'Student Portfolio',
          client_secret: 'KumZap6qXBBmQDs6ajw3iVv24zHHNBoR',
          grant_type: 'password',
          username,
          password,
        }),
      ),
    );
    return data;
  }
      validateAndDecodeAccessToken(accessToken: string): JwtPayload {
        try {
          const decodedToken = this.jwtService.verify(accessToken) as JwtPayload;
          return decodedToken;
        } catch (error) {
          throw new Error('Invalid access token');
        }
      }
        // Method to decode the access token
        decodeAccessToken(accessToken: string): JwtPayload {
          try {
            const decodedToken = jwt.decode(accessToken) as JwtPayload;
            return decodedToken;
          } catch (error) {
            throw new Error('Error decoding access token');
          }
        }
}
