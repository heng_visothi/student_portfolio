// // auth.controller.ts

// import { Controller, Post, Body, HttpException, HttpStatus } from '@nestjs/common';
// import { AuthService } from './auth.service';

// @Controller('auth')
// export class AuthController {
//   constructor(private readonly authService: AuthService) {}

//   @Post('login')
//   async login(@Body('username') username: string, @Body('password') password: string){
//     try {
//       const token = await this.authService.login(username, password);
//       return { access_token: token };
//     } catch (error) {
//       throw new HttpException('Invalid credentials', HttpStatus.UNAUTHORIZED);
//     }
//   }
  
// }
// auth.controller.ts
import { Controller, Post, Body, HttpException, HttpStatus } from '@nestjs/common';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  async login(@Body('username') username: string, @Body('password') password: string) {
    try {
      const token = await this.authService.login(username, password);
      const accessToken = token.access_token;

      // Decode the access token to log and inspect its structure
      const decodedToken = this.authService.decodeAccessToken(accessToken);
      console.log('Decoded Token:', decodedToken);

      // Extract user SID or any other information you need
      const userSid = decodedToken ? decodedToken.sid : null;

      return { access_token: token, user_sid: userSid };
    } catch (error) {
      throw new HttpException('Invalid credentials', HttpStatus.UNAUTHORIZED);
    }
  }
}

