import { School } from "src/modules/schools/entities/school.entity";
import { TransactionDetail } from "src/modules/transaction_details/entities/transaction_detail.entity";
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity({name:'transactions'})
export class Transaction {
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    transaction_type:string;

    @Column()
    price:string;

    @Column()
    date:Date;

    @Column()
    status:string;

    @ManyToOne(()=>School , (school)=>school.id)
    school:School;

    @OneToMany(()=>TransactionDetail, (transaction_detail)=>transaction_detail.id)
    transaction_detail:TransactionDetail[];

}
