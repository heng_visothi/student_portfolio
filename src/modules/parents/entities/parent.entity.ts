// src/modules/parents/entities/parent.entity.ts
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm';
import { User } from 'src/modules/users/entities/user.entity';

@Entity({ name: 'parents' })
export class Parent {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  sex: string;

  @Column()
  PhoneNumber: string;

  @Column()
  nationally: string;

  @Column()
  workplace: string;

  @Column()
  career: string;

  @Column()
  race: string;

  @Column()
  alive: boolean;

  @ManyToOne(() => User, (user) => user.parents)
  @JoinColumn({ name: 'user_id' })
  user: User;
}
