import { Major } from "src/modules/majors/entities/major.entity";
import { StudentSubject } from "src/modules/student_subjects/entities/student_subject.entity";
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity({name:'subject'})
export class Subject {
    @PrimaryGeneratedColumn()
    id:number;
    
    @Column()
    name:string;

    @Column()
    description: string;

    @ManyToOne(()=>Major, (major)=>major.id)
    major:Major

    @OneToMany(()=>StudentSubject , (student_subject)=>student_subject.id)
    student_subject:StudentSubject[];



}
