import { Module } from '@nestjs/common';
import { MajorsService } from './servicecs/majors.service';
import { MajorsController } from './controllers/majors.controller';

@Module({
  controllers: [MajorsController],
  providers: [MajorsService],
})
export class MajorsModule {}
