import { School } from "src/modules/schools/entities/school.entity";
import { Subject } from "src/modules/subjects/entities/subject.entity";
import { Column, Entity, JoinColumn, JoinTable, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity({name:'majors'})
export class Major {
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    name:string;

    // @OneToOne(()=>School, (School)=>School.)
    //one to many for table subject
   @OneToMany(()=>Subject , (subject)=>subject.id)
   subject:Subject[];

   //one to one table School 
    @OneToOne(()=>School)
    @JoinColumn()
    school:School;

}
