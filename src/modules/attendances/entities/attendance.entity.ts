import { AttendancesDetail } from "src/modules/attendances_details/entities/attendances_detail.entity";
import { StudentSubject } from "src/modules/student_subjects/entities/student_subject.entity";
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity({name:'attendances'})
export class Attendance {

    @PrimaryGeneratedColumn()
    id: string;

    // chacker
    // @Column()
    // checker_id:string;

    @Column({})
    class:string;


    @Column()
    start_at:Date;

    @Column()
    end_at:Date;

    //manytoOne

    @ManyToOne(()=>StudentSubject,(student_subject)=>student_subject.id)
    student_subject:StudentSubject;
    
    @OneToMany(()=>AttendancesDetail,(attendance_details)=>attendance_details.id)
    attendance_details:AttendancesDetail[];




}
