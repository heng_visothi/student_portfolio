import { Transaction } from "src/modules/transactions/entities/transaction.entity";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity({name:'transaction_detial'})
export class TransactionDetail {
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    payment_method:string;

    @Column()
    amount:string;

    @Column()
    payer:string;   

    @Column()
    payment_date:Date;

    @ManyToOne(()=>Transaction,(transaction)=>transaction.id)
    transaction:Transaction;
}
