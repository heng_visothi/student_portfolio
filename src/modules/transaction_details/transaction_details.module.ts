import { Module } from '@nestjs/common';
import { TransactionDetailsService } from './services/transaction_details.service';
import { TransactionDetailsController } from './controllers/transaction_details.controller';

@Module({
  controllers: [TransactionDetailsController],
  providers: [TransactionDetailsService],
})
export class TransactionDetailsModule {}
