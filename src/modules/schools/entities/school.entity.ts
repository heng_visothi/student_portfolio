import { Transaction } from "src/modules/transactions/entities/transaction.entity";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity({name:'schools'})
export class School {
    @PrimaryGeneratedColumn()
    id:string;

    @Column()
    name:string;
    
    @Column()
    adddress:string;

    @OneToMany(()=>Transaction, (transaction)=>transaction.id)
    transaction:Transaction;


}
