import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  JoinTable,
  ManyToMany,
  BeforeInsert,
  BeforeUpdate,
  CreateDateColumn,
  OneToMany,
} from 'typeorm';

import * as bcrypt from 'bcrypt';
import { Parent } from 'src/modules/parents/entities/parent.entity';
import { AttendancesDetail } from 'src/modules/attendances_details/entities/attendances_detail.entity';

@Entity({ name: 'users' })
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  username: string;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  // Other fields you may want to include
  // @Column({ nullable: true })
  // fullName: string;

  // @Column({ nullable: true })
  // profile_img: string;

  @OneToMany(() => AttendancesDetail, (attendanceDetail) => attendanceDetail.users)
  attendance_details?: AttendancesDetail[];

  @ManyToMany(() => Parent)
  @JoinTable()
  parents?: Parent[];

  @CreateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  @CreateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP', onUpdate: 'CURRENT_TIMESTAMP' })
  updatedAt: Date;

  @BeforeInsert()
  async hashPassword(): Promise<void> {
    this.password = await bcrypt.hash(this.password, 10);
  }

  async validatePassword(password: string): Promise<boolean> {
    return bcrypt.compare(password, this.password);
  }
}
