import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { AttendancesDetailsService } from './attendances_details.service';
import { CreateAttendancesDetailDto } from './dto/create-attendances_detail.dto';
import { UpdateAttendancesDetailDto } from './dto/update-attendances_detail.dto';

@Controller('attendances-details')
export class AttendancesDetailsController {
  constructor(private readonly attendancesDetailsService: AttendancesDetailsService) {}

  @Post()
  create(@Body() createAttendancesDetailDto: CreateAttendancesDetailDto) {
    return this.attendancesDetailsService.create(createAttendancesDetailDto);
  }

  @Get()
  findAll() {
    return this.attendancesDetailsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.attendancesDetailsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateAttendancesDetailDto: UpdateAttendancesDetailDto) {
    return this.attendancesDetailsService.update(+id, updateAttendancesDetailDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.attendancesDetailsService.remove(+id);
  }
}
