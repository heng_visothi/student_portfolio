import { Injectable } from '@nestjs/common';
import { CreateAttendancesDetailDto } from './dto/create-attendances_detail.dto';
import { UpdateAttendancesDetailDto } from './dto/update-attendances_detail.dto';

@Injectable()
export class AttendancesDetailsService {
  create(createAttendancesDetailDto: CreateAttendancesDetailDto) {
    return 'This action adds a new attendancesDetail';
  }

  findAll() {
    return `This action returns all attendancesDetails`;
  }

  findOne(id: number) {
    return `This action returns a #${id} attendancesDetail`;
  }

  update(id: number, updateAttendancesDetailDto: UpdateAttendancesDetailDto) {
    return `This action updates a #${id} attendancesDetail`;
  }

  remove(id: number) {
    return `This action removes a #${id} attendancesDetail`;
  }
}
