import { PartialType } from '@nestjs/mapped-types';
import { CreateAttendancesDetailDto } from './create-attendances_detail.dto';

export class UpdateAttendancesDetailDto extends PartialType(CreateAttendancesDetailDto) {}
