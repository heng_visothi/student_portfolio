import { Attendance } from "src/modules/attendances/entities/attendance.entity";
import { User } from "src/modules/users/entities/user.entity";
import { Column, Entity, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity({name:'attendance_details'})
export class AttendancesDetail {
    @PrimaryGeneratedColumn()
    id:number; 
    //absent , present or late .....
    @Column()
    status:string;

    @Column()
    reason:string;

    @Column()
    noted:string;

    @ManyToOne(()=>User,(users)=>users.id)
    users:User;
    @ManyToOne(()=>Attendance,(attendances)=>attendances.id)
    attendances:Attendance;
}
