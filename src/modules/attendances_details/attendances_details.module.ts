import { Module } from '@nestjs/common';
import { AttendancesDetailsService } from './attendances_details.service';
import { AttendancesDetailsController } from './attendances_details.controller';

@Module({
  controllers: [AttendancesDetailsController],
  providers: [AttendancesDetailsService],
})
export class AttendancesDetailsModule {}
