import { Attendance } from "src/modules/attendances/entities/attendance.entity";
import { Subject } from "src/modules/subjects/entities/subject.entity";
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity({name:'student_subject'})
export class StudentSubject {
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    name: string;   

    @Column()
    enrollment_date: Date;

    //true it means class completed and false it means class not completed 
    @Column()
    status:boolean;

    @ManyToOne(()=>Subject , (subject)=>subject.id)
    subject:Subject;

    @OneToMany(()=>Attendance,(attendance)=>attendance.id)
    attendance:Attendance[];
    
    





}
