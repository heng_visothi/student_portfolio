import { Module } from '@nestjs/common';
import { StudentSubjectsService } from './services/student_subjects.service';
import { StudentSubjectsController } from './controllers/student_subjects.controller';

@Module({
  controllers: [StudentSubjectsController],
  providers: [StudentSubjectsService],
})
export class StudentSubjectsModule {}
