// src/common/interceptors/format-response.interceptor.ts
import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class FormatResponseInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      map((data) => {
        // Check if the data is an object
        if (typeof data === 'object' && data !== null) {
          return { success: true, data };
        }

        // If the data is not an object, create a new object with a "data" property
        return { success: true, data: { result: data } };
      }),
    );
  }
}
